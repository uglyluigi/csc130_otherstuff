###########################################################################################
# Name: Brennan Forrest
# Date: 10/29/17
# Description: Creates a random list of numbers of a specific size
###########################################################################################

from random import randrange as newrandom

# function that prompts the user for a list size, minimum and maximum values, creates the list, and returns it
# you must use the list functions discussed in class to add integers to the list
def fill_list():
	list = []
	SIZE = int(input("How many random integers would you like to add to the list? "))
	MIN = int(input("What would you like the minimum value to be? "))
	MAX = int(input("What would you like the maximum value to be? "))

	i = 0
	while i < SIZE:
		list.append(newrandom(MIN, MAX + 1))
		i += 1

	return list



# function that receives the list as a parameter, and calculates and returns the mean
"""
Returns the average of all values in a list of integers.
"""
def mean(int_list):
	sum = 0

	for i in int_list: #Add all the integers in the list together and divide by the length of the list.
		sum += i

	return 1.0 * sum / len(int_list)

# function that receives the list as a parameter, and calculates and returns the median
"""
Returns the median value in a list of integers.
"""
def median(int_list):
	list_length = len(int_list)
	sorted_list = sorted(int_list) #Now, I know I asked about sorting the lists, but I was talking about using it for all other functions as well. Median requires a sorted list though.
	list_mid_index = (list_length - 1) // 2 #Finds the index of the element located in the center of the list. Floored division just in case the list is of an even size.

	if len(int_list) % 2 == 0:
		return (sorted_list[list_mid_index] + sorted_list[list_mid_index + 1]) / 2 #Averages the two middle integers in the case of an even-sized list.
	else:
		return sorted_list[list_mid_index]


# function that receives the list as a parameter, and calculates and returns the range
"""
Finds the range of the list.
Simply subtracts the minimum from the maximum value of the list.
"""
def range(int_list):
	return max(int_list) - min(int_list)


###############################################
# MAIN PART OF THE PROGRAM
# implement the main part of your program below
# comments have been added to assist you
###############################################
# create the list
nums = fill_list()

# display the list
# there is no need to write/call your own function for this part
print("The list: {}".format(str(nums)))

# calculate and display the mean
print("The mean of the list is {}.".format(mean(nums)))

# calculate and display the median
print("The median of the list is {}.".format(nums))


# calculate and display the range
print("The range of the list is {}.".format(range(nums)))
