###########################################################################################
# Name: Brennan Forrest
# Date: 
# Description: 
##################################################################################-

# "the snippet" implemented iteratively
def passSomeBeersIteravely(n):
	while (n > 0):
		print "{} bottles of beer on the wall.".format(n)
		print "{} bottles of beer.".format(n)
		print "Take one down, pass it around."
		n = n - 1
		print "{} bottles of beer on the wall.".format(n)
		print

#you are like little baby watch this
def passSomeBeersRecursively(n):
	bottles = "bottles"

	if n == 1:
		bottles = "bottle
"
	print "{} {} of beer on the wall.".format(n, bottles)
	print "{} {} of beer.".format(n, bottles)
	print "Take one down, pass it around."

	n -= 1

	if n != 1:
		bottles = "bottles"
	elif n == 1:
		bottles = "bottle"

	print "{} {} of beer on the wall.\n".format(n, bottles)

	if n > 0:
		passSomeBeersRecursively(n)


	

###############################################
# MAIN PART OF THE PROGRAM
###############################################
#passSomeBeersIteravely(99)
passSomeBeersRecursively(99)

