###########################################################################################
# Name: Brennan Forrest
# Date: 10/26/17	
# Description: Creates a fabulous list and tells you about it.
###########################################################################################

#Fills the list with however many integers the user wants.
def fill_list():
	list = []
	LIST_SIZE = input("How many integers would you like to add to the list? ")

	while len(list) < int(LIST_SIZE):
		list.append(input("Enter an integer: "))

	return list


#Lovingly copied from Casual Statistics
"""
Returns the maximum values in a list of integers.
"""
def minimum(int_list):
	min = int_list[0]
	index = 0
	while index < len(int_list) - 1: #Iterate over the list until the index reaches len(list) - 1 since lists are 0-based
		if int_list[index] < min:
			min = int_list[index] #Set the newly found minimum
		index += 1
	return min

"""
Returns the maximum value in a list of integers.
"""
def maximum(int_list):
	max = int_list[0]
	index = 0
	while index < len(int_list) - 1:
		if int_list[index] > max:
			max = int_list[index]
		index += 1
	return max


###############################################
# MAIN PART OF THE PROGRAM
# implement the main part of your program below
# comments have been added to assist you
###############################################
# create the list
nums = fill_list()

# display information about the list
print("The original list: {}".format(str(nums)))
print("The length of the list is {}.".format(len(nums)))
print("The minimum value in the list is {}.".format(min(nums)))
print("The maximum value in the list is {}.".format(max(nums)))
print("The sorted list: {}".format(str(sorted(nums))))
print("The reverse sorted list: {}".format(str(sorted(nums, reverse=True))))
