from math import log, floor


#x % 5 is a set repeating 0-4 every 5 nums
#maybe involve floor(log10(x)) to figure out which power of 10 is involved
#for every power of 10 there's a distance of 10 ^ (floor(log10(x)) - 1) where
#10^x has x zeroes
#10^x + 1 has x - 1 zeroes
#nothing changes until you get to 
#each 10^x has x zeroes
#log10(x)
#maybe add to log10(x) + (x % 10)
#1000 has 3 zeroes 10^3
#1001 has 2 zeroes 10^3 + 1
#1010 has 2 zeroes 10^3 + 10
#1011 has 1 zero 10^3 + 11
#1020 has 2 zeroes 10^3 + 20
#1021 has 1 zero 10^3 + 21


for x in range(1, 1000000):
	print ("{} % 5 = {} | {} % (5 * log10(x)) = {}".format(x, x % 5, x, x % int(floor((5 * log(x))))))